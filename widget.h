#ifndef WIDGET_H
#define WIDGET_H

#include <QtWidgets>
#include "actions.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(int width,int height, QWidget *parent = nullptr);
    void paintEvent( QPaintEvent *event );

    QGroupBox* createListGroupBox();
    QGroupBox* createButtonsGroupBox();
    QGroupBox* createResultsGroupBox();
    QGroupBox* createInfoGroupBox();

    ~Widget();

private slots:
    void generateSlot();
    void sortSlot();

private:
    Ui::Widget *ui;
    Actions actions;
    Sortings sortings;

    void clear();

    QGroupBox *listGroupBox;
    QListWidget *list;
    QTableWidget *table;

    QGroupBox *buttonsGroupBox;
    QComboBox *generateType,*sortType;

    QGroupBox *resultsGroupBox;
    QGroupBox *infoGroupBox;

    QLabel *labels[3];
};

#endif // WIDGET_H
