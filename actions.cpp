#include "actions.h"
#include <ctime>
#include <cmath>

Actions::Actions(){
     vecExample.resize(10);
     vecFull.resize(3);
}

void Actions::generateExample(){
    srand(time(nullptr));

    for (size_t i = 0;i < vecExample.size();i++){
        vecExample[i] = rand()%1000+1;
    }
};

void Actions::generate(){
    srand(time(nullptr));
    size_t size[] = {10000,150000,1000000};

    for (size_t i = 0; i < vecFull.size();i++){
        vecFull[i].resize(size[i]);
        for(size_t j = 0; j < size[i]; j++)
            vecFull[i][j] = rand()%100000+1;
    }


}

Actions::~Actions(){}
