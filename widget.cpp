#include "widget.h"
#include "ui_widget.h"
#include "actions.h"
#include <vector>
#include "shareddefs.h"

Widget::Widget(int width,int height,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    setFixedSize(QSize(width,height));
    setWindowFlag(Qt::MSWindowsFixedSizeDialogHint);
    setStyleSheet("QWidget {font-family: 'Century Gothic'; font: 13pt bold ; color:#000000	}");

    QGridLayout *mainLayout = new QGridLayout(this);

    mainLayout->addWidget(createListGroupBox(),0,0);
    mainLayout->addWidget(createButtonsGroupBox(),0,1);
    mainLayout->addWidget(createResultsGroupBox(),1,0);
    mainLayout->addWidget(createInfoGroupBox(),1,1);

    setLayout(mainLayout);

}

void Widget::paintEvent( QPaintEvent *event )
{
    QPainter painter(this);
    painter.drawPixmap( 0, 0, QPixmap(":/images/trianglify.png").scaledToHeight(height()));

    QWidget::paintEvent(event);
}

void Widget::generateSlot(){
    int index = generateType->currentIndex();

    if(index == 0 )
    {
        list->clear();
        list->show();
        table->hide();

        actions.generateExample();
        for(auto i: actions.vecExample){
            list->addItem(QString::number(i));
        }
    }
    else
    {
        table->show();
        list->hide();

        actions.generate();
        for(size_t i = 0; i < actions.vecFull.size(); i++){
            bool t = Actions::sorted(actions.vecFull[i].begin(),actions.vecFull[i].end());
            table->setItem(i,1, new QTableWidgetItem(t? "true": "false"));

        }

    }


}

void Widget::clear(){
    sortings.oprt = 0;
    list->clear();
    for (size_t i = 0; i < 3; ++i)
    {
        labels[i]->clear();
    }
}

void Widget::sortSlot(){
    int generateIndex = generateType->currentIndex();
    int sortIndex = sortType->currentIndex();

    if(generateIndex == 0){
        clear();
        Actions::sort(Sorts(sortIndex), actions.vecExample.begin(),actions.vecExample.end());
        labels[0]->setText("10: " + QString::number(sortings.oprt));

        for(auto i: actions.vecExample){
            list->addItem(QString::number(i));
        }
    }

    else{
        clear();
        QString str[3];
        for(size_t i = 0; i < actions.vecFull.size(); i++){
            Actions::sort(Sorts(sortIndex), actions.vecFull[i].begin(),actions.vecFull[i].end());
            labels[i]->setText(QString::number(actions.vecFull[i].size()) + ": " + QString::number(sortings.oprt));

            bool t = Actions::sorted(actions.vecFull[i].begin(),actions.vecFull[i].end());
            table->setItem(i,1, new QTableWidgetItem(t? "true": "false"));
        }
    }


}

QGroupBox* Widget::createListGroupBox()
{
    listGroupBox = new QGroupBox("Numbers",this);
    QGridLayout *layout = new QGridLayout(this);

    list = new QListWidget(this);

    table = new QTableWidget(3,2,this);
    table->horizontalHeader()->setVisible(false);
    table->horizontalHeader()->setStretchLastSection(true);
    table->verticalHeader()->setVisible(false);
    table->setItem(0,0, new QTableWidgetItem(QString::number(10000)));
    table->setItem(1,0, new QTableWidgetItem(QString::number(150000)));
    table->setItem(2,0, new QTableWidgetItem(QString::number(1000000)));
    table->hide();

    layout->addWidget(list,0,0);
    layout->addWidget(table,0,0);

    listGroupBox->setLayout(layout);
    listGroupBox->setFixedSize(QSize(this->width()/2, this->height()/15*7));
    return listGroupBox;
}

QGroupBox* Widget::createButtonsGroupBox()
{
    buttonsGroupBox = new QGroupBox("Actions",this);
    QVBoxLayout *layout = new QVBoxLayout(this);

    QPushButton *generateButton = new QPushButton("Generate",this);
    generateType = new QComboBox(this);
    generateType->addItem("Example");
    generateType->addItem("Full Test");

    QPushButton *sortButton = new QPushButton("Sort",this);
    sortType = new QComboBox(this);
    sortType->addItem("Bubble Sort");//TODO
    sortType->addItem("Merge Sort");
    sortType->addItem("Couting Sort");//TODO
    sortType->addItem("Radix Sort");

    setStyleSheet("QPushButton {background-color: white;border-style: outset;border-width: 2px;border-radius: 10px;border-color: beige;padding: 6px;}");
    setStyleSheet("QPushButton:pressed {background-color: rgb(224, 0, 0);border-style: inset;}");

    connect(generateButton, SIGNAL(clicked()),this,SLOT(generateSlot()));
    connect(sortButton, SIGNAL(clicked()),this,SLOT(sortSlot()));

    layout->addWidget(generateType);
    layout->addWidget(generateButton);
    layout->addWidget(sortType);
    layout->addWidget(sortButton);

    buttonsGroupBox->setLayout(layout);
    return buttonsGroupBox;
}

QGroupBox* Widget::createResultsGroupBox()
{
    resultsGroupBox = new QGroupBox("Results",this);
    QVBoxLayout *layout = new QVBoxLayout(this);

    for (size_t i = 0; i < 3; ++i)
    {
        labels[i] = new QLabel(this);
        layout->addWidget(labels[i]);
    }

    resultsGroupBox->setLayout(layout);
    return resultsGroupBox;
}

QGroupBox* Widget::createInfoGroupBox()
{
    infoGroupBox = new QGroupBox("Info",this);
    QVBoxLayout *layout = new QVBoxLayout(this);

    QLabel *label1 = new QLabel("CP:QuadCore Intel Core i5-8250U",this);
    QLabel *label2 = new QLabel("RAM: 8070 МБ  (DDR4 SDRAM)", this);
    QLabel *label3 = new QLabel("ОS: Microsoft Windows 10 Pro", this);

    layout->addWidget(label1);
    layout->addWidget(label2);
    layout->addWidget(label3);

    infoGroupBox->setLayout(layout);
    return infoGroupBox;
}

Widget::~Widget()
{
    delete ui;
}
