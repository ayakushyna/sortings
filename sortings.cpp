#include "sortings.h"
#include <cmath>

Sortings::Sortings() {
    oprt = 0;
}

unsigned long Sortings::oprt;

Sortings::~Sortings(){}
\
/*
template <class Iterator>
void Sortings::bubbleSort(Iterator first, Iterator last) {
    int n = std::distance(first, last);

    bool swapped = true;
    for(int i = 0; swapped; i++)
    {
        swapped = false;
        for (int j = 0; j < n - i - 1; j++)
        {
            if (*(first+j) > *(first+j+1))
            {
                std::swap(*(first + j), *(first + j + 1));
                swapped = true;
            }
        }
    }
}

template<class Iterator>
void Sortings::merge(Iterator first, Iterator last, size_t low, size_t high) {
    int n = std::distance(first, last);
    int* B = new int[n];

    for (size_t i = low; i <= high; i++)
        B[i] = *(first + i);

    size_t left = low,
        mid = (low + high) / 2,
        right = mid + 1;


    size_t i = low;
    for (; left <= mid && right <= high; ++i) {
        if (B[left] <= B[right])
        {
            *(first + i) = B[left];
            ++left;
        }
        else
        {
            *(first + i) = B[right];
            ++right;
        }
    }

    while (left <= mid) {
        *(first + i) = B[left];
        ++left;
        ++i;
    }

    while (right <= high) {
        *(first + i) = B[right];
        ++right;
        ++i;
    }

    delete[] B;
}

template<class Iterator>
void Sortings::mergeSort(Iterator first, Iterator last,size_t low, size_t high) {
    if (low < high) {
        size_t mid = (low + high) / 2;
        mergeSort(first, last, low, mid);
        mergeSort(first, last, mid + 1, high);
        merge(first, last, low, high);
    }
}

template <class Iterator>
int Sortings::maxNumber(Iterator first, Iterator last) {

    int max = *first;

    for (auto i = first + 1; i != last; ++i) {
        if (*i > max) max = *i;
    }

    return max;
}

template <class Iterator>
void Sortings::countingSort(Iterator first, Iterator last) {
    int m = maxNumber(first, last),
        n = std::distance(first, last);

    int* B = new int[n];
    int* C = new int[m + 1]();

    for (int i = 0; i < n; ++i)
    {
        C[*(first + i)]++;
    }

    for (int i = 1; i <= m; ++i)
    {
        C[i] += C[i-1];
    }

    for (int i = n-1; i >= 0; --i)
    {
        --C[*(first + i)];
        B[C[*(first + i)]] = *(first + i);
    }

    std::copy(B, B + n, first);
    delete[] B;
    delete[] C;
}

int Sortings::digit(int x, int i) {
    while (i > 1) {
        x /= 10;
        --i;
    }

    return x %= 10;
}

template <class Iterator>
int Sortings::maxDigits(Iterator first, Iterator last) {
    int max = maxNumber(first, last),
        digits = 0;

    while (max) {
        max /= 10;
        ++digits;
    }

    return digits;
}

template <class Iterator>
void Sortings::radixSort(Iterator first, Iterator last ) {
    int m = maxDigits(first, last),
        n = std::distance(first, last);

    for (int i = 1; i <= m; ++i) {
        int* B = new int[n];
        int C[10] = {};

        for (int j = 0; j < n; ++j)
        {
            int d = digit(*(first + j), i);
            ++C[d];
        }

        int count = 0;

        for (int j = 0; j <= 9; ++j)
        {
            int tmp = C[j];
            C[j] = count;
            count += tmp;
        }

        for (int j = 0; j < n; ++j)
        {
            int d = digit(*(first + j), i);
            B[C[d]] = *(first + j);
            ++C[d];
        }

        std::copy(B, B + n, first);
        delete[] B;
    }
}
    */

