#ifndef SORTINGS_H
#define SORTINGS_H

#include <iostream>

class Sortings
{
public:
    Sortings();

    ~Sortings();

    static unsigned long oprt;

    template <class Iterator>
    static void bubbleSort(Iterator first, Iterator last){
        int n = std::distance(first, last);

        bool swapped = true;
        oprt++;
        for(int i = 0; swapped; i++)
        {
            swapped = false;
            oprt++;
            for (int j = 0; j < n - i - 1; j++)
            {
                if (*(first+j) > *(first+j+1))
                {
                    std::swap(*(first + j), *(first + j + 1));
                    swapped = true;
                    oprt+=7;
                }
                oprt+=4;
            }
        }
    }

    template <class Iterator>
    static void mergeSort(Iterator first, Iterator last,size_t low, size_t high){
        if (low < high) {
            size_t mid = (low + high) / 2;
            oprt+=5;

            mergeSort(first, last, low, mid);
            oprt+=4;
            mergeSort(first, last, mid + 1, high);
            oprt+=5;
            merge(first, last, low, high);
            oprt+=4;
        }
        oprt+=4;
    }

    template <class Iterator>
    static void countingSort(Iterator first, Iterator last){
        int m = maxNumber(first, last),
            n = std::distance(first, last);

        int* B = new int[n];
        int* C = new int[m + 1]();

        for (int i = 0; i < n; ++i)
        {
            C[*(first + i)]++;
            oprt+=2;
        }

        for (int i = 1; i <= m; ++i)
        {
            C[i] += C[i-1];
            oprt+=3;
        }

        for (int i = n-1; i >= 0; --i)
        {
            --C[*(first + i)];
            oprt+=3;
            B[C[*(first + i)]] = *(first + i);
            oprt+=4;
        }

        std::copy(B, B + n, first);
        oprt+= 2*n;

        delete[] B;
        delete[] C;
    }

    template <class Iterator>
    static void radixSort(Iterator first, Iterator last ){
        int m = maxDigits(first, last),
            n = std::distance(first, last);

        for (int i = 1; i <= m; ++i) {
            int* B = new int[n];
            int C[10] = {};

            for (int j = 0; j < n; ++j)
            {
                int d = digit(*(first + j), i);
                oprt+=3;
                ++C[d];
                oprt+=2;
            }

            int count = 0;
            oprt++;

            for (int j = 0; j <= 9; ++j)
            {
                int tmp = C[j];
                C[j] = count;
                count += tmp;
                oprt+=8;
            }

            for (int j = 0; j < n; ++j)
            {
                int d = digit(*(first + j), i);
                oprt+=3;
                B[C[d]] = *(first + j);
                oprt+=4;
                ++C[d];
                oprt+=2;
            }

            std::copy(B, B + n, first);
            oprt+= 2*n;

            delete[] B;
        }
    }

private:
    template <class Iterator>
    static void merge(Iterator first, Iterator last, size_t low, size_t high){
        int n = std::distance(first, last);
        int* B = new int[n];

        for (size_t i = low; i <= high; i++) {
            B[i] = *(first + i);
            oprt+=3;
        }

        size_t left = low,
            mid = (low + high) / 2,
            right = mid + 1;

        oprt+=10;

        size_t i = low;
        oprt++;
        for (; left <= mid && right <= high; ++i) {
            if (B[left] <= B[right])
            {
                *(first + i) = B[left];
                oprt+=3;
                ++left;
                oprt+=2;

            }
            else
            {
                *(first + i) = B[right];
                oprt+=3;
                ++right;
                oprt+=2;
            }
             oprt+=4;
        }

        while (left <= mid) {
            oprt+=3;

            *(first + i) = B[left];
            oprt+=3;
            ++left;
            oprt+=2;
            ++i;
            oprt+=2;
        }

        while (right <= high) {
            oprt+=3;

            *(first + i) = B[right];
            oprt+=3;
            ++right;
            oprt+=2;
            ++i;
            oprt+=2;
        }

        delete[] B;
    }

    template <class Iterator>
    static int maxNumber(Iterator first, Iterator last){
        int max = *first;

        for (auto i = first + 1; i != last; ++i) {
            if (*i > max) max = *i;
        }

        return max;
    }

    template <class Iterator>
    static int maxDigits(Iterator first, Iterator last){
        int max = maxNumber(first, last),
            digits = 0;

        while (max) {
            max /= 10;
            ++digits;
        }

        return digits;
    }

    static int digit(int x, int i){
        while (i > 1) {
            oprt+=3;
            x /= 10;
            oprt+=2;
            --i;
            oprt+=2;
        }

        return x %= 10;
    }
};

#endif // SORTINGS_H
