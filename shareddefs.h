#ifndef SHAREDDEFS_H
#define SHAREDDEFS_H

enum Sorts {
    BUBBLE ,
    MERGE,
    COUNTING,
    RADIX
};

#endif // SHAREDDEFS_H
