#ifndef ACTIONS_H
#define ACTIONS_H

#include "shareddefs.h"
#include "sortings.h"
#include <vector>

class Actions
{
public:
    Actions();

    std::vector<int> vecExample;
    std::vector<std::vector<int>> vecFull;

    void generateExample();
    void generate();

    template <class Iterator>
    static void sort(Sorts type, Iterator first, Iterator last){
        switch(type){
        case BUBBLE:
        {
            Sortings::bubbleSort(first, last);
            break;
        }
        case MERGE:
        {
            Sortings::mergeSort(first, last, 0, std::distance(first, last)-1);
            break;
        }
        case COUNTING:
        {
            Sortings::countingSort(first, last);
            break;
        }
        case RADIX:
        {
            Sortings::radixSort(first, last);
            break;
        }
        }
    }

    template <class Iterator>
    static bool sorted(Iterator first, Iterator last){
        for(auto i = first; i != last-1; i++){
            if(*i > *(i+1))
            {
                return false;
            }
        }
        return true;
    }

    ~Actions();
};

#endif // ACTIONS_H
